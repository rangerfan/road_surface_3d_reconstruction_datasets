### Road Surface 3D Reconstruction Based on Dense Subpixel Disparity Map Estimation ###

This repository is created by Rui Fan.  

The datasets are used for stereo vision-based road surface 3D reconstruction and pothole detection.

### Website ###

More details about the project are provided at: http://www.ruirangerfan.com

### Datasets ###

There are three datasets: dataset 1, dataset 2, dataset 3.

dataset 1 and 2 aim at the 3D recontruction of a road scenery, while the dataset 3 is used to evaluate the accuracy of the reconstruction results. The uncalibrated images and calibration parameters are used to reconstruct the 3D scenary in Matlab.

### Videos ###

The experimental videos are available at: https://www.youtube.com/channel/UC1DgZTYD19jaLFzgtNMznDg/playlists 

### Contact ###

ranger_fan@outlook.com
